package com.c0d3in3.lecture5

class User(val firstName: String, val lastName: String, val userAge: Int, val userLanguages: List<String>)
