package com.c0d3in3.lecture5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init() // initializing code
    }

    private fun init(){
        val languages = arrayOf("Kotlin", "Java", "C++", "C#", "Python") // initialize random languages array
        val names = arrayOf("John", "Tedo", "Damian", "Gela", "Jane", "Jessie", "Skyler") // initialize random names array
        val lastNames = arrayOf("Versetti", "Manvelidze", "McDonald", "Gnolidze", "Versace", "Ivanov", "White") // initialize random last names array

        generateButton.setOnClickListener { //trigger button
            updateUI(User(names.random(), lastNames.random(), Random.nextInt(18, 30), listOf(languages.random(), languages.random(), languages.random()).distinct())) // call updateUI method with random user
        }

    }

    private fun updateUI(user : User){
        userFirstName.text = "User's first name: ${user.firstName}" // update user's first name TextView
        userLastName.text = "User's last name: ${user.lastName}" // update user's last name TextView
        userAge.text = "User's age: ${user.userAge}" // update user's age TextView
        userLanguages.text = "User's languages: ${user.userLanguages}" // update user's languages TextView
    }
}
